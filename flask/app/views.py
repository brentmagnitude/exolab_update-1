from flask import render_template, flash, redirect
from app import app
from .forms import LoginForm
import subprocess
import time
import strandtest
import LCD
import shutil

entry_WPA="""
network={{
\tssid=\"{}\"
\tpriority=2
\tpsk=\"{}\"
\tkey_mgmt=WPA-PSK
}}
"""

entry_NONE="""
network={{
\tssid=\"{}\"
\tpriority=2
\tkey_mgmt=NONE
}}
"""

entry_PAEP="""
network={{
\tssid=\"{}\"
\tpriority=2
\tkey_mgmt=WPA-EAP
\tproto=RSN
\tpairwise=CCMP
\tauth_alg=OPEN
\teap=PEAP
\tidentity=\"{}\"
\tpassword=\"{}\"
\tphase1="peaplabel=0"
\tphase2="auth=MSCHAPV2"
}}
"""

# index view function suppressed for brevity
@app.route('/', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        print form.restart
        print form.clear
        if form.restart.data:
            accepted = True
            if (form.security.data == 'WPA-PSK' and len(form.psk.data)>=8 ):
                new_entry=entry_WPA.format(form.ssid.data,form.psk.data,form.security.data)
            elif (form.security.data == 'WPA-PSK' and len(form.psk.data) < 8 ):
                flash('Passkey is not long enough')
                accepted = False
            elif (form.security.data=='NONE'):
                new_entry=entry_NONE.format(form.ssid.data,form.security.data)
            elif (form.security.data=='WPA-PAEP'):
                new_entry=entry_PAEP.format(form.ssid.data,form.username.data,form.password.data)

            if(accepted):
                flash('Credentials accepted. Restarting...')
                wpa=open("/etc/wpa_supplicant/wpa_supplicant.conf",'a')
                wpa.write(new_entry)
                wpa.close()
                LCD.start()
                LCD.lcd_string("RESTARTING...",LCD.LCD_LINE_1)
                LCD.lcd_string("PLEASE WAIT...",LCD.LCD_LINE_2)
                subprocess.Popen('/home/pi/ExoLab/APclean.sh')
            
        elif form.clear.data:
            shutil.copyfile("/home/pi/ExoLab/wpa_backup.conf","/etc/wpa_supplicant/wpa_supplicant.conf")
            LCD.start()
            LCD.lcd_string("CLEARING NTWKS ",LCD.LCD_LINE_1)
            LCD.lcd_string("AND RESTARTING ",LCD.LCD_LINE_2)
            subprocess.Popen('/home/pi/ExoLab/APclean.sh')

        elif form.backup.data:
            LCD.start()
            LCD.lcd_string("RESTORING",LCD.LCD_LINE_1)
            LCD.lcd_string("FACTORY CONFIG",LCD.LCD_LINE_2)
            subprocess.Popen('/home/pi/backup/installer.sh')
            # The installer script will run APclean.sh - 9/18/18 LK
            #subprocess.Popen('/home/pi/ExoLab/APclean.sh')

        
        return redirect('/')
    return render_template('login.html', 
                           title='WPA-2 Credentials',
                           form=form)
